function all2int(input) {
  return parseInt(input||0, 10);
}

function all2float(input) {
  return parseFloat(input||.0, 10);
}

function Lot(lot, comissions) {
  this.lot = lot;

  this.lot["buyer-"] = comissions;

  return this;
}
var lotProto = Lot.prototype;
var april1st = (new Date("2014-03-31T13:00:00.000Z")).getTime();

Object.defineProperty(lotProto, "fivepercent", {
  get: function() {
    return ((new Date(this.lot.date)).getTime() >= april1st)
      ? Math.round(all2int(this.lot.push) * 0.08)
      : Math.round(all2int(this.lot.push) * 0.05)
    ;
  }
});

Object.defineProperty(lotProto, "total-check", {
  get: function() {
    return all2int(this.lot.push) + all2int(this.fivepercent) + all2int(this.lot["auction-tax"]) + all2int(this.lot.recycle) + all2int(this.lot.toll);
  }
});

Object.defineProperty(lotProto, "total-jpy-cost", {
  get: function() {
    return all2int(this["total-check"]) + all2int(this.lot.shipping) + all2int(this.lot.freight);
  }
});

Object.defineProperty(lotProto, "total-jpy-cost-in-rur", {
  get: function() {
    return Math.round(all2int(this["total-jpy-cost"]) * all2float(this.lot.jpy2rur * 0.0102));
  }
});

Object.defineProperty(lotProto, "bank-fee", {
  get: function() {
    return Math.round(all2int(this["total-jpy-cost-in-rur"]) * 0.0075);
  }
});

Object.defineProperty(lotProto, "total-rur-cost", {
  get: function() {
    return Math.round(all2int(this.lot["pre-duty"]) * all2float(this.lot.usd2rur) + all2int(this.lot["recycle-ru"]) + all2int(this["bank-fee"]) + all2int(this.lot.broker) + all2int(this.lot.storage) + all2int(this.lot["individual-cost"]) + all2int(this.lot["legal-entity-cost"]));
  }
});

Object.defineProperty(lotProto, "total-jpy-and-rur-cost", {
  get: function() {
    return all2int(this["total-jpy-cost-in-rur"]) + all2int(this["total-rur-cost"]);
  }
});

Object.defineProperty(lotProto, "commission-jp", {
  get: function() {
    return all2int(this.lot["buyer-"]["commission-jp"]);
  }
});

Object.defineProperty(lotProto, "total-jpy-price", {
  get: function() {
    return all2int(this.lot.push) + all2int(this["commission-jp"]);
  }
});

Object.defineProperty(lotProto, "total-jpy-price-in-rur", {
  get: function() {
    return Math.round(all2int(this["total-jpy-price"]) * all2float(this.lot.jpy2rur * 0.0102));
  }
});

Object.defineProperty(lotProto, "commission-ru", {
  get: function() {
    return all2int(this.lot["buyer-"]["commission-ru"]);
  }
});

Object.defineProperty(lotProto, "total-rur-price", {
  get: function() {
    return Math.round(all2int(this.lot["pre-duty"]) * all2float(this.lot.usd2rur) + all2int(this["commission-ru"]) + all2int(this.lot["recycle-ru-price"]) + all2int(this.lot["individual-price"])) + all2int(this.lot["legal-entity-price"]);
  }
});

Object.defineProperty(lotProto, "total-jpy-and-rur-price", {
  get: function() {
    return all2int(this["total-jpy-price-in-rur"]) + all2int(this["total-rur-price"]) ;
  }
});

Object.defineProperty(lotProto, "self-cost", {
  get: function() {
    return all2int(this.lot.push) + all2int(this.lot["auction-tax"] + all2int(this.lot.shipping) + all2int(this.lot.freight));
  }
});

Object.defineProperty(lotProto, "sub-formatted", {
  get: function() {
    return require("util").format(
      "Себестоимость: %d<br/> Всего по чекам: %d<br/> Заплатил клиент: %d<br/> Полная стоимость: %d<br/> Разница: %d"
      , this["self-cost"]
      , this["total-check"]
      , this["total-jpy-price"]
      , this["total-jpy-cost"]
      , all2int(this["total-jpy-price"]) - this["self-cost"]
    );
  }
});

Object.defineProperty(lotProto, "sub", {
  get: function() {
    return all2int(this["total-jpy-price"]) - this["self-cost"];
  }
});

module.exports = Lot;
