$(function() {
  var Datastore = require("nedb")
    , setImmediate = global.setImmediate
    , async = require("async")
    , _ = require("underscore")

    , dbOnloadHandler = function(err) {
      if (err) {
        setTimeout(function() {
          location.reload();
        }, 1000);
      }
    }

    , db = window.db = {
      clients: new Datastore({
        filename: "./data/clients.db"
        , autoload: true
        , onload: dbOnloadHandler
      })

      , lots: new Datastore({
        filename: "./data/lots.db"
        , autoload: true
        , onload: dbOnloadHandler
      })

      , currencies: new Datastore({
        filename: "./data/currencies.db"
        , autoload: true
        , onload: dbOnloadHandler
      })

      , "client-groups": new Datastore({
        filename: "./data/client-groups.db"
        , autoload: true
        , onload: dbOnloadHandler
      })

      , commissions: new Datastore({
        filename: "./data/commissions.db"
        , autoload: true
        , onload: dbOnloadHandler
      })

      , comings: new Datastore({
        filename: "./data/comings.db"
        , autoload: true
        , onload: dbOnloadHandler
      })
    }

    , clients = {}
    , clientsByName = {}

    , clientGroups = {}
    , clientGroupsByMetaname = {}

    , currencies = {}

    , tableName
    , table
    , tableFields

    , newLotDefaults = {
      freight: 29000
      , "recycle-ru": 3150
      , broker: 2500
      , storage: 4000
    }

    , Lot = require("./assets/js/lotProto")

    , showAdminItems = true

    , format = require("util").format
    , option = '<option value="%s" %s>%s</option>'
    , optionTemplate = $('<option value=""></option>')
    , selectTemplate = $("<select class='related-field'><option value=''>не выбран</option></select>")
    , insertRowTemplate = {}

    , relatedCache = {}
    , getRelatedTableProxy = function(related, field, value, editable, cb) {
      var cacheName = format("%s-%s", related, field)
        , cached
        , div = $("<div></div>")
      ;

      if (cacheName in relatedCache && (cached = relatedCache[cacheName]) && (Date.now() - cached.at < 1000*30)) {
        var select = cached.select.clone();

        select
          .find("[selected]")
            .removeAttr("selected")
            .end()
          .find(format("[value='%s']", value))
            .attr("selected", "selected")
            .end()
        ;

        process.nextTick(function() {
          return editable ? cb(select) : cb(div.text(select.find("[selected]").text()));
        });
      } else {
        var select = selectTemplate.clone()
          , options = []

          , query = {}
          , sort = {}
        ;
        sort[field] = 1;

        db[related].find(query).sort(sort).exec(function(err, relatedRows) {
          _.each(relatedRows, function(relatedRow) {
            var newOption = optionTemplate.clone()
              .attr("value", relatedRow._id)
              .text(relatedRow[field])
            ;

            if (value === relatedRow._id ? "selected" : "") {
              newOption.attr("selected", "selected");
            }

            options.push(newOption);
          });

          select.append(options);

          relatedCache[cacheName] = {
            at: Date.now()
            , select: select
          }

          process.nextTick(function() {
            return editable ? cb(select) : cb(div.text(select.find("[selected]").text()));
          });
        });
      }
    }

    , getCommissionsReal = function(date, clientGroup, cb) {
      db.commissions.findOne({
        date: {
          $lte: moment(date).endOf("day").toDate().toJSON()
        }
        , "client-group": clientGroup
      }).sort({ date: -1 }).exec(function(err, commission) {
        var commissions = _.defaults(commission||{}, {
          "commission-jp": 0
          , "commission-ru": 0
          , "individual-cost": 0
          , "individual-price": 0
        });

        process.nextTick(function() {
          cb(err, commissions);
        });
      });
    }
    , getCommissionsProxy = async.memoize(getCommissionsReal, function(a, b) {
      return format("%s-%s", a, b);
    })

    , spanTemplate = $("<span></span>")

    , tableCargo = async.cargo(function(rows, callback) {
      trashcanCargo.pause();

      setImmediate(function() {
        async.eachSeries(rows, function(row, nextRow) {
          process.nextTick(function() {
            if (tableName === "lots") {
              var client;

              getCommissionsProxy(row.date, (client = clients[row.buyer]) ? client["client-group"] : -1, function(err, commission) {
                var lot = new Lot(row, commission);

                _.each(Object.keys(tableTotals), function(key) {
                  return tableTotals[key].push(+(lot[key] || row[key]));
                });

                insertRow(lot, nextRow);
              });
            } else {
              insertRow(row, nextRow);
            }
          });
        }, function(err) {
          process.nextTick(function() {
            $(".loading-records").addClass("hide");
            callback();
          });
        });
      });
    }, 10)

    , tableTotals

    , trashcanCargo = async.cargo(function(items, callback) {
      process.nextTick(function() {
        async.each(items, function(item, next) {
          process.nextTick(function() {
            item.remove();
            item = null;
            next();
          });
        }, callback);
      });
    }, 50)

    , exportedLotsPath
  ;

  tableCargo.drain = function renderTableTotals() {
    process.nextTick(function() {
      if (tableName !== "lots") {
        return;
      }

      var totals = $($("#template-totals-lots-" + tableFields).html())
        , tableNode = $(".lots")
        , tbodyNode = tableNode.find("tbody")
      ;

      totals.find("[data-field]").each(function() {
        var $this = $(this)
          , field = $this.data("field")
          , values = field in tableTotals ? tableTotals[field] : []

          , i = 0
          , count = values.length
          , result = 0
        ;

        for ( ; i < count; i++) {
          result += values[i];
        }

        $this.text(result);
      });

      tbodyNode.append(totals);
    });
    trashcanCargo.resume();
  }

  _.each(db, function(dbTable, dbName) {
    dbTable.persistence.setAutocompactionInterval(60*1000);
    dbTable.ensureIndex({
      fieldName: "date"
    });

    if (dbName === "lots") {
      insertRowTemplate[dbName + "-both"] = $($("#template-tr-" + dbName + "-both").html())
      insertRowTemplate[dbName + "-ru"] = $($("#template-tr-" + dbName + "-ru").html())
      insertRowTemplate[dbName + "-jp"] = $($("#template-tr-" + dbName + "-jp").html())
    } else {
      insertRowTemplate[dbName] = $($("#template-tr-" + dbName).html())
    }
  });

  db.clients.ensureIndex({
    fieldName: "name"
  });
  db.lots.ensureIndex({
    fieldName: "body"
  });
  db.lots.ensureIndex({
    fieldName: "lot-number"
  });

  $(document)
    .on("keyup", function(e) {
      if (e.keyCode === 82 && e.ctrlKey) {
        require("nw.gui").Window.get().reloadDev();
      }
    })
    .on("focus", "[contenteditable]", function() {
      var $this = $(this);
      $this.data("before", $this.html());
    })
    .on("blur keyup paste input", "[contenteditable]", function() {
      var $this = $(this);
      if ($this.data("before") !== $this.html()) {
        $this.data("before", $this.html());
        $this.trigger("change", [true]);
      }
    })

    .on("change", "[contenteditable], .related-field", function(e, isContenteditable) {
      var $this = $(this)
        , attr = (isContenteditable ? $this : $this.parents("td")).attr("class").replace(/^item-/, "")
        , value = isContenteditable ? $this.html() : $this.val()
        , rows = $(this).parents("tr")
        , id = rows.data("item-id")

        , set = {}
      ;
      set[attr] = value;

      table.update({ _id: id }, { $set: set }, {}, function(err, numUpdated) {
        if (err) {
          console.error(err);
          return;
        }

        if ("lot" in rows.data("lot")) {
          rows.data("lot").lot[attr] = value;
          rows.trigger("calculate");
        }

        if (tableName === "commissions") {
          process.nextTick(function() {
            _.each(getCommissionsProxy.memo, function(value, key, collection) {
              delete collection[key];
            });
          });
        }
      });
    })

    .on("click", ".add-new-row .add", function(e) {
      e.preventDefault();
      var picker = $(this).parents("form").find(".date")
        , value = picker.editable("getValue", true)
        , date = value.startOf("day").toDate().toJSON()
        , now = moment().toDate().toJSON()
        , defaults = { date: date, createdAt: now }
      ;

      if (tableName === "lots") {
        defaults = $.extend(defaults, newLotDefaults);
      }

      table.insert(defaults, function(err, row) {
        if (err) {
          console.error(err);
          return;
        }

        $(".no-records").addClass("hide");

        if (tableName === "lots") {
         var lot = new Lot(
            row
            , {
              "commission-jp": 0
              , "commission-ru": 0
            }
          );

          insertRow(lot, function() {});
          return;
        }

        insertRow(row, function() {});
      });
    })

    .on("click", ".drop-db", function(e) {
      e.preventDefault();

      bootbox.confirm("Вы уверены?", function(yes) {
        if (yes) {
          table.remove({}, { multi: true }, function(err, numRemoved) {
            table.loadDatabase(function(err) {
              $(document).trigger("1c:page-filtered");
            });
          });
        }
      });
    })

    .on("click", ".delete-row", function(e) {
      e.preventDefault();

      var $this = $(this)
        , id = $this.parents("tr").data("item-id")
        , rows = $(".item-id-" + id)
      ;

      bootbox.confirm("Вы уверены?", function(yes) {
        if (yes) {
          table.remove({ _id: id }, {}, function(err, numRemoved) {
            if (err) {
              console.error(err);
              return;
            }

            rows.remove();
            var number = 1;
            $(".lots .item-number span").each(function() {
              $(this).text(number++);
            });
          });
        }
      });
    })

    .on("calculate", "tr", function(e) {
      var $this = $(this)
        , id = $this.data("item-id")
        , rows = $(".item-id-" + id)
      ;

      process.nextTick(function() {
        table.findOne({ _id: id }, function(err, row) {
          if (err) {
            console.error(err);
            return;
          }

          var formulas = rows.find("[data-calculate]")
            , buyerRelation = rows.find("[data-relation-buyer]")
            , nextStep = function() {
              formulas.each(function() {
                var $this = $(this);

                process.nextTick(function() {
                    var attr = $this.attr("class").replace(/^item-/, "")
                      , data = rows.data("lot")
                    ;

                    if (data) {
                      var result = data[attr];

                      if (attr === "sub-formatted") {
                        $this.html(result);
                      } else {
                        $this.text(result);
                      }
                      row[attr] = result;
                    }
                });
              });
            }
          ;

          if (buyerRelation.length) {
            var client;

            getCommissionsProxy(row.date, (client = db.clients.indexes._id.getMatching(row.buyer)) ? client[0] && client[0]["client-group"] : -1, function(err, commission) {
              row["buyer*"] = commission;

              nextStep(err);
            });
          } else {
            nextStep();
          }
        });
      });
    })

    .on("relations", "tr", function(e) {
      var $this = $(this)
      ;

      process.nextTick(function() {
        $this.find("[data-relation]").each(function() {
          var node = $(this)
            , label = node.find("span")
            , value = label.text()
            , related = node.data("relation")
            , field = node.data("field")
          ;

          getRelatedTableProxy(related, field, value, tableName === "lots" ? false : true, function(select) {
            process.nextTick(function() {
              node.append(select);
            });
          });
        });
      });
    })

    .on("click", ".navbar-brand", function(e) {
      e.preventDefault();
      $(".navbar-nav a[data-role=main]").click();
    })

    .on("click", ".navbar-nav.pages a", function(e) {
      e.preventDefault();
      var $this = $(this)
        , name = $this.data("table-name")
        , fields = $this.data("fields")
        , li = $this.parents("li")
        , pageBody = $(".page-body")
        , grafoniy = $(".grafoniy")
        , grafoniyLi = grafoniy.parents("li").removeClass("active")
      ;

      $(document)
        .trigger("1c:page-loaded", [{
          name: name
          , fields: fields
        }])
        .trigger("1c:page-loaded-" + name)
      ;

      li
        .addClass("active")
        .siblings()
          .removeClass("active")
      ;
    })

    .on("click", ".download-lots", function(e) {
      e.preventDefault();

      bootbox.alert("<h3>Загружаю…</h3>");

      var success = function() {
          bootbox.hideAll().alert("<h3>Импорт завершён</h3>");
          $(".navbar-brand").click();
        }

        , fail = function() {
          bootbox.hideAll().alert("<h3>Ошибка импорта</h3>");
          $(".navbar-brand").click();
        }
      ;

      $.getJSON("http://dvline.ru/jp_accounts/makeDataJson.php", {})
        .done(function(data) {
          async.series({
            "get-and-update-comings": function(nextImportStep) {
              process.nextTick(function() {
                var comings = data.comings;
                async.each(comings, function(coming, nextComing) {
                  process.nextTick(function() {
                    db.comings.update({
                      date: moment(coming.name).toDate().toJSON()
                      , name: coming.name
                      , id: +coming.id
                    }, { $set: {
                      "num-of-cars": +coming.NumOfCars
                    }}, { upsert: true }, nextComing);
                  });
                }, nextImportStep);
              });
            }

            , "process-lots": function(nextImportStep) {
              var lots = data.lots;
              async.eachSeries(lots, function(lot, nextLot) {
                async.waterfall(require('nwglobal').Array(
                  function findClientOrInsertNew(nextStep) {
                    db.clients.findOne({
                      name: lot.logins_name
                    }, function(err, client) {
                      if (!err && client) {
                        nextStep(null, client);
                      } else {
                        db.clients.insert({
                          date: moment().startOf("day").toDate().toJSON()
                          , createdAt: moment().toDate().toJSON()
                          , name: lot.logins_name
                          , "client-group": clientGroupsByMetaname[(+lot.logins_id === 3 ? "self" : +lot.isDealer ? "dealer" : "usual")]._id
                        }, function(err, client) {
                          clientsByName[client.name] = client;
                          clients[client._id] = client;

                          nextStep(err, client);
                        });
                      }
                    });
                  }

                  , function insertIndividualCostAndPrice(client, nextStep) {
                    if (+lot.logins_id === 3) {
                      getCommissionsProxy(moment(lot.buy_date).startOf("day").toDate().toJSON(), client["client-group"], function(err, commissions) {
                        nextStep(null, _.extend(client, {
                          "individual-cost": commissions["individual-cost"]
                          , "individual-price": commissions["individual-price"]
                        }));
                      });
                    } else {
                      nextStep(null, client);
                    }
                  }

                  , function findLotOrInsertNew(client, nextStep) {
                    db.lots.findOne({
                      body: lot.bodynumber
                      , "lot-number": lot.lotnumber
                    }, function(err, savedLot) {
                      if (!err && savedLot) {
                        var coming = +lot.id_coming;

                        if (coming === savedLot.coming) {
                          nextStep(null, client, savedLot);
                        } else {
                          db.lots.update({
                            _id: savedLot._id
                          }, {
                            coming: coming
                          }, function(err) {
                            nextStep(err, client, savedLot);
                          });
                        }
                      } else {
                        db.lots.insert(_.extend(newLotDefaults, {
                          date: moment(lot.buy_date).startOf("day").toDate().toJSON()
                          , articul: lot.articul
                          , createdAt: moment().toDate().toJSON()
                          , body: lot.bodynumber
                          , auction: lot.aucname
                          , "lot-number": lot.lotnumber
                          , "model-and-year": lot.name + "<br />" + lot.year
                          , push: +lot.push
                          , recycle: +lot.resaikle
                          , shipping: +lot.transport
                          , toll: +lot.roadtax
                          , "auction-tax": +lot.auctax
                          , buyer: client._id
                          , "pre-duty": +lot["pre-duty"]
                          , usd2rur: +lot.usd
                          , jpy2rur: +lot.yen
                          , "individual-cost": client["individual-cost"]||0
                          , "individual-price": client["individual-price"]||0
                          , coming: +lot.id_coming
                        }), nextStep);
                      }
                    });
                  }
                ), nextLot);
              }, function(err) {
                if (!err) {
                  success();
                } else {
                  fail();
                }
              });
            }
          });
        })
        .fail(fail)
      ;
    })

    .on("click", ".export-lots", function(e) {
      e.preventDefault();

      var input = $("#save-file-dialog")
        , saver = input.clone()
      ;

      saver
        .attr("nwsaveas", format("Экспорт лотов от %s.json", moment().format("L")))
        .change(function(e) {
          var $this = $(this)
            , file = $this.val()
            , fs = require("fs")
            , path = require("path")
            , saveTo = path.resolve(file)
          ;

          db.lots.find({ date: {
            $gte: ($(".filter-date.date-from").data("editable").value || moment()).toDate().toJSON()
            , $lte: ($(".filter-date.date-to").data("editable").value || moment()).toDate().toJSON()
          }}).sort({date: 1, createdAt: 1}).exec(function(err, rows) {
            if (err) {
              debugger;
            }

            fs.writeFile(saveTo, JSON.stringify(rows, null, 2), function(err) {
              if (err) {
                debugger;
              }

              exportedLotsPath = saveTo;
              bootbox.alert('<h3>Экспорт завершён</h3> <a href="#" class="btn btn-default" id="open-exported-lots-folder">Открыть папку с файлом</a>');
            });
          });
        })
        .click()
      ;
    })

    .on("click", ".import-lots", function(e) {
      e.preventDefault();

      var input = $("#open-file-dialog")
        , opener = input.clone()
        , importing = $(this).data("from")

        , fields = {
          jp: [
            "auction-tax"
            , "recycle"
            , "toll"
            , "shipping"
            , "freight"
          ]

          , ru: [
            "recycle-ru"
            , "bank-fee"
            , "broker"
            , "storage"
            , "individual-cost"
            , "individual-price"
          ]
        }
      ;

      opener
        .change(function(e) {
          var $this = $(this)
            , file = $this.val()
            , fs = require("fs")
            , path = require("path")
            , importFrom = path.resolve(file)
          ;

          fs.readFile(importFrom, {
            encoding: "utf-8"
          }, function(err, data) {
            if (err) {
              return;
            }

            var rows = JSON.parse(data);
            async.each(rows, function(row, nextRow) {
              process.nextTick(function() {
                var set = {};
                _.each(fields[importing], function(field) {
                  set[field] = row[field];
                });

                db.lots.update({
                  body: row.body
                }, { $set: set }, {}, function(err) {
                  return nextRow(err);
                });
              });
            }, function(err) {
              bootbox.alert("<h3>Импорт завешён</h3>");
              $(".navbar-brand").click();
            });
          });
        })
        .click()
      ;
    })

    .on("1c:page-loaded", function(e, data) {
      $(".editable").editable({language: "ru", onblur: "submit"});

      tableName = data.name;
      table = db[tableName];
      tableFields = data.fields;

      var tableContainer = $(".table-container").show()
        , chartContainer = $(".chart-container").hide()
        , tableNode = $(".lots")
        , theadNode = tableNode.find("thead").empty()
        , tbodyNode = tableNode.find("tbody")

        , theadTemplate = $("#template-thead-" + tableName + (tableName === "lots" ? "-" + tableFields : "")).html()

        , removeTrash = function() {
          var removedChildren = tbodyNode.children().hide().remove();

          if (removedChildren.length) {
            trashcanCargo.pause();
            trashcanCargo.push(require("nwglobal").Array.prototype.slice.call(removedChildren));
          }
        }
      ;

      removeTrash();
      $(".loading-records").removeClass("hide");

      $(theadTemplate).appendTo($(".lots thead"));

      function getClients(nextStep) {
        process.nextTick(function() {
          db.clients.find({}, function(err, rows) {
            if (!err) {
              rows.forEach(function(row) {
                clients[row._id] = row;
                clientsByName[row.name] = row;
              });
            }

            nextStep(err);
          });
        });
      }
      getClients.__proto__ = require('nwglobal').Function;

      function getClientGroups(nextStep) {
        process.nextTick(function() {
          db["client-groups"].find({}, function(err, rows) {
            if (!err) {
              rows.forEach(function(row) {
                clientGroups[row._id] = row;
                clientGroupsByMetaname[row.metaname] = row;
              });
            }

            nextStep(err);
          });
        });
      }
      getClientGroups.__proto__ = require('nwglobal').Function;

      function getCurrencies(nextStep) {
        process.nextTick(function() {
          db.currencies.find({}, function(err, rows) {
            if (!err) {
              async.each(rows, function(row, nextRow) {
                currencies[moment(row.date).format("L")] = {
                  jpy2rur: Number((row.jpy2rur||"").replace(",", ".")) / 100
                  , usd2rur: Number((row.usd2rur||"").replace(",", "."))
                  // , eur2rur: Number((row.usd2rur||"").replace(",", "."))
                }

                nextRow();
              }, function(err) {
                nextStep(err);
              })
            }
          });
        });
      }
      getCurrencies.__proto__ = require('nwglobal').Function;

      function getLots(nextStep) {
        process.nextTick(function() {
          var filter = {}
            , dateFilter = { date: {
              $gte: ($(".filter-date.date-from").data("editable").value || moment()).toDate().toJSON()
              , $lte: ($(".filter-date.date-to").data("editable").value || moment()).toDate().toJSON()
            }}
            , comingFilter = { coming: +$(".coming-chooser").val() }
            , resultFilter = _.extend(filter, tableName === "currencies" ? dateFilter : tableName === "lots" ? { $and: [dateFilter, comingFilter] } : {})
          ;

          table.find(resultFilter).sort({date: 1, createdAt: 1}).exec(function(err, rows) {
            if (err) {
              console.error(err);
              return;
            }

            var old = tableTotals;
            old = null;

            tableTotals = {
              push: []
              , fivepercent: []
              , "auction-tax": []
              , recycle: []
              , toll: []
              , "total-check": []
              , shipping: []
              , freight: []
              , "total-jpy-cost": []
              , "total-jpy-cost-in-rur": []
              , "pre-duty": []
              , "total-rur-cost": []
              , "total-jpy-and-rur-cost": []
              , "sub": []
            };

            if (rows.length) {
              $(".no-records").addClass("hide");
              tableCargo.pause();
              var oldDrain = tableCargo.drain;
              tableCargo.drain = function() {
                removeTrash();
                tableCargo.push(rows);
                rows = null;
                tableCargo.drain = oldDrain;
              }
              tableCargo.clear();
              tableCargo.resume();
            } else {
              $(".loading-records").addClass("hide");
              $(".no-records").removeClass("hide");
            }
          });
        });
      }
      getLots.__proto__ = require('nwglobal').Function;

      async.auto({
        "get-clients": getClients
        , "get-client-groups": getClientGroups
        , "get-currencies": getCurrencies
        , "get-lots": require('nwglobal').Array("get-clients", "get-client-groups", "get-currencies", getLots)
      });
    })

    .on("1c:page-filtered", function(e) {
      $(document).trigger("1c:page-loaded", [{
        name: tableName
        , fields: tableFields
      }]);
    })

    .on("click", ".grafoniy", function(e) {
      e.preventDefault();
      var $this = $(this)
        , navbarLis = $(".navbar-nav.pages li").removeClass("active")
        , grafoniyLi = $this.parents("li").addClass("active")
        , tableContainer = $(".table-container").hide()
        , chartContainer = $(".chart-container").show()

        , dateFilter = {
          $gte: ($(".filter-date.date-from").data("editable").value || moment()).toDate().toJSON()
          , $lte: ($(".filter-date.date-to").data("editable").value || moment()).toDate().toJSON()
        }

        , chartData = {
          costs: []
          , fivepercent: []
          , recycle: []
          , toll: []
          , returns: []
        }

        , chartDataTotals = {
          cost: 0
          , check: 0
          , price: 0
          , "full-cost": 0

          , fivepercent: 0
          , recycle: 0
          , toll: 0
          , returns: 0

          , count: 0
        }
        , preChartData = {}
      ;

      async.series({
        "get-lots": function(nextStep) {
          process.nextTick(function() {
            db.lots.find({ date: dateFilter }, function(err, lots) {
              if (!err) {
                async.each(lots, function(lot, nextLot) {
                  var date = moment(lot.date).format("L");

                  if (!(date in preChartData)) {
                    preChartData[date] = {
                      cost: []
                      , check: []
                      , price: []
                      , "full-cost": []

                      , fivepercent: []
                      , recycle: []
                      , toll: []
                      , returns: []
                    };
                  }

                  process.nextTick(function() {
                    async.waterfall(require("nwglobal").Array(
                      function getCommissions(calculate) {
                        getCommissionsProxy(lot.date, (client = db.clients.indexes._id.getMatching(lot.buyer)) ? client[0]["client-group"] : -1, function(err, commission) {
                          calculate(err, commission);
                        });
                      }

                      , function calculateChartData(commission, next) {
                        var lot_ = new Lot(lot, commission);

                        var cost = lot_["self-cost"] // себестоимость
                          , check = lot_["total-check"] // всего по чекам
                          , price = all2int(lot.push) + lot_["commission-jp"] // заплатил клиент
                          , fullCost = lot_["total-jpy-cost"] // полная стоимость

                          , returns = lot_.fivepercent + all2int(lot.recycle) + all2int(lot.toll) // возвраты
                        ;

                        preChartData[date].cost.push(cost);
                        preChartData[date].check.push(check);
                        preChartData[date].price.push(price);
                        preChartData[date]["full-cost"].push(fullCost);

                        preChartData[date].fivepercent.push(lot_.fivepercent);
                        preChartData[date].recycle.push(all2int(lot.recycle));
                        preChartData[date].toll.push(all2int(lot.toll));
                        preChartData[date].returns.push(returns);

                        next();
                      }
                    ), nextLot);
                  });
                }, nextStep);
              } else {
                nextStep(err);
              }
            });
          });
        }

        , "compute-chart-data": function(nextStep) {
          process.nextTick(function() {
            var dates = Object.keys(preChartData)
            ;

            async.each(dates, function(date, nextDate) {
              process.nextTick(function() {
                var data = preChartData[date]
                  , i = 0
                  , count = data.cost.length
                  , cost = 0
                  , check = 0
                  , price = 0
                  , fullCost = 0

                  , fivepercent = 0
                  , recycle = 0
                  , toll = 0
                  , returns = 0

                  , dateObject = moment(date, "L").toDate()
                  , dateLabel = format("%s (%d)", date, count)
                ;

                for (; i < count; i++) {
                  cost += data.cost[i];
                  chartDataTotals.cost += data.cost[i];

                  check += data.check[i];
                  chartDataTotals.check += data.check[i];

                  price += data.price[i];
                  chartDataTotals.price += data.price[i];

                  fullCost += data["full-cost"][i];
                  chartDataTotals["full-cost"] += data["full-cost"][i];

                  fivepercent += data.fivepercent[i];
                  chartDataTotals.fivepercent += data.fivepercent[i];

                  recycle += data.recycle[i];
                  chartDataTotals.recycle += data.recycle[i];

                  toll += data.toll[i];
                  chartDataTotals.toll += data.toll[i];

                  returns += data.returns[i];
                  chartDataTotals.returns += data.returns[i];

                  chartDataTotals.count++;
                }

                chartData.costs.push({
                  date: dateObject
                  , dateLabel: dateLabel
                  , cost: cost
                  , check: check
                  , price: price
                  , "full-cost": fullCost
                });

                chartData.fivepercent.push({
                  date: dateObject
                  , dateLabel: dateLabel
                  , fivepercent: fivepercent
                });
                chartData.recycle.push({
                  date: dateObject
                  , dateLabel: dateLabel
                  , recycle: recycle
                });
                chartData.toll.push({
                  date: dateObject
                  , dateLabel: dateLabel
                  , toll: toll
                });
                chartData.returns.push({
                  date: dateObject
                  , dateLabel: dateLabel
                  , returns: returns
                });

                data = null;
                nextDate();
              });
            }, nextStep);
          });
        }

        , "draw-chart": function(nextStep) {
          setImmediate(function() {
            function sortedChartData(data) {
              return data.sort(function(a, b) {
                return a.date < b.date ? -1 : 1;
              });
            }

            var charts = require("nwglobal").Array(
              {
                title: "Возвраты"
                , data: chartData.returns
                , node: $(".chart-returns")
                , commonSeriesSettings: { type: "bar" }
                , series: [[ "returns", "Должно вернуться" ]]
              }

              , {
                title: "Полная стоимость / Заплатил клиент / Всего по чекам / Себестоимость"
                , data: chartData.costs
                , node: $(".chart-costs")
                , commonSeriesSettings: { type: "bar" }
                , series: [[ "full-cost", "Полная стоимость" ], [ "price", "Заплатил клиент" ], [ "check", "Всего по чекам" ], [ "cost", "Себестоимость" ]]
              }

              , {
                title: "8 процентов"
                , data: chartData.fivepercent
                , node: $(".chart-fivepercent")
                , series: [[ "fivepercent", "8 процентов" ]]
                , legendInside: true
              }

              , {
                title: "Ресайкл"
                , data: chartData.recycle
                , node: $(".chart-recycle")
                , series: [[ "recycle", "Ресайкл" ]]
                , legendInside: true
              }

              , {
                title: "Дорожный налог"
                , data: chartData.toll
                , node: $(".chart-toll")
                , series: [[ "toll", "Дорожный налог" ]]
                , legendInside: true
              }
            );

            async.each(charts, function(chart, nextChart) {
              setImmediate(function() {
                chart.node.dxChart({
                  dataSource: sortedChartData(chart.data)
                  , title: chart.title

                  , commonSeriesSettings: _.defaults(chart.commonSeriesSettings||{}, {
                    argumentField: "dateLabel"
                    , type: "line"
                    , tagField: "date"
                  })

                  , series: _.map(chart.series, function(serie) {
                    var label = serie.pop()
                      , attr = serie.pop()
                      , name = format("%s <b>%s</b>", label, Number(chartDataTotals[attr]).format())
                    ;

                    return { valueField: attr, name: name };
                  })

                  , tooltip: {
                    enabled: true
                    , customizeText: function() {
                      return Number(this.value).format();
                    }
                  }

                  , legend: (function() {
                    return chart.legendInside ? {
                      position: "inside"
                      , paddingTopBottom: 0
                      , paddingLeftRight: 0
                    } : {};
                  })()

                  , pointDblClick: function() {
                    var date = moment(this.tag);

                    $(".filter-date.date-from").editable("setValue", date);
                    $(".filter-date.date-to").editable("setValue", date);

                    setTimeout(function() {
                      $(".navbar-brand").click();
                    }, 100);
                  }
                });

                nextChart();
              });
            }, function(err) {
              $(".costs-sub .sub").text(Number(chartDataTotals.price - chartDataTotals.cost).format());
              $(".total-autos .total").text(Number(chartDataTotals.count).format());
            });

            process.nextTick(function() {
              $(".chart-summary").dxPieChart({
                dataSource: [
                  { label: "Полная стоимость", value: chartDataTotals["full-cost"] }
                  , { label: "Заплатил клиент", value: chartDataTotals.price }
                  , { label: "Всего по чекам", value: chartDataTotals.check }
                  , { label: "Себестоимость", value: chartDataTotals.cost }
                ]
                , title: "Соотношения"

                , series: [{
                  argumentField: "label",
                  valueField: "value",
                  label: {
                    visible: true,
                    connector: {
                      visible: true,
                      width: 1
                    }
                    , customizeText: function() {
                      return Number(this.value).format();
                    }
                  }
                }]
              });
            });

            nextStep();
          });
        }
      });
    })

    .on("click", "#open-exported-lots-folder", function(e) {
      e.preventDefault();

      require("nw.gui").Shell.showItemInFolder(exportedLotsPath);
    })
  ;

  function insertRow(row, cb) {
    var tableRoot = $("table.lots")
      , table = tableRoot.find("tbody")
      , realLot = (row.lot || row)
      , template = insertRowTemplate[tableName + (tableName === "lots" ? "-" + tableFields : "")].clone()
    ;

    async.series({
      "insert-values": function(nextStep) {
        async.each(Object.keys(realLot), function(attr, nextAttr) {
          process.nextTick(function() {
            if (realLot.hasOwnProperty(attr)) {
              var node = template.find(".item-" + attr)
                , result = realLot[attr]
              ;

              if (node.length) {
                if (attr === "date") {
                  result = moment(result).format('LL');
                }

                if (node.data("relation")) {
                  node.append(spanTemplate.clone().append(result));
                } else if (attr === "model-and-year" || attr === "notes") {
                  node.html(result);
                } else {
                  node.text(result);
                }
              }
            }

            nextAttr();
          });
        }, nextStep);
      }

      , "insert-row": function(nextStep) {
        process.nextTick(function() {
          template
            .find(".item-number")
              .find("span")
                .text($(".lots .item-number span").length + 1)
                .end()
              .end()
            .appendTo(table)
            .data({
              "item-id": realLot._id
              , "lot": row
            })
            .addClass("item-id-" + realLot._id)
            .trigger("relations")
            .trigger("calculate")
          ;

          nextStep();
        });
      }
    }, function(err) {
      process.nextTick(function() {
        cb(err);
      });
    });
  }

  function all2int(input) {
    return parseInt(input||0, 10);
  }

  Number.prototype.format = function(n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1 ');
  };

  if (!showAdminItems) {
    $(".admin-item").hide();
  }

  $.fn.editable.defaults.mode = "inline";
  moment.lang("ru");

  $(".editable").editable({language: "ru", onblur: "submit"});
  $(".filter-date.date-from").editable("setValue", moment().startOf("month"));
  $(".filter-date.date-to").editable("setValue", moment().endOf("month"));
  $(".date-add").editable("setValue", moment());

  $(".navbar-brand").click();

  var pageFilteredHandler = function(e, params) {
    window.setTimeout(function() {
      var grafoniy = $(".grafoniy")
        , grafoniyLi = grafoniy.parents("li")
      ;

      if (grafoniyLi.hasClass("active")) {
        grafoniy.click();
      } else {
        $(document).trigger("1c:page-filtered");
      }
    }, 100);
  }

  $(".filter-date").on("save", pageFilteredHandler);
  $(".coming-chooser").on("change", pageFilteredHandler);

  process.nextTick(function() {
    var select = $(".coming-chooser")
      , option = $("<option value></option>")

      , defaultOption = option.clone()
        .text("Не выбран")
        .val(0)
        .appendTo(select)
    ;

    db.comings.find({}).sort({date: -1}).exec(function(err, comings) {
      async.eachSeries(comings, function(coming, nextComing) {
        process.nextTick(function() {
          if (coming.name === "0000-00-00") {
            return;
          }

          var newOption = option.clone();

          newOption
            .text(format("%s (%d)", coming.name, coming["num-of-cars"]))
            .val(coming.id)
            .appendTo(select)
          ;

          nextComing();
        });
      });
    });
  });
});
